import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:another_xlider/another_xlider.dart';
import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:csounddart/csound.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:path_provider/path_provider.dart';
import 'Globals.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Csound'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isPlaying = false;

  late Csound _cs = Csound();

  static const String _exampleCsd = """
 <CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================




<CsInstruments>

sr      =       48000
ksmps   =       64
nchnls =       2
0dbfs   =       1

<CsControls>
{
  "first_row": {
    "type": "row",
    
    "slcontrol": {
      "type": "slider",
      "min": 200,
      "max": 500, 
      "value": 200,
      "width": 400
    },
    
    "first_button": {
      "type": "button"
    },
    
    "first_spin": {
      "type": "spinbox",
      "min": 1.0,
      "max": 5.0,
      "step": 1.5,
      "value": 1.0,
      "width": 150,
      "decimals": 1
    }
  },
  
  "sec_slider": {
    "type": "slider",
    "min": 10,
    "max": 200, 
    "step": 10,
    "value": 12
  },
  "mytoggle": {
    "type": "toggle",
    "value": 1
  }
  
}
</CsControls>

instr 1 
// instr
kfq = chnget:k("slcontrol")
ao = vco2(0.3, kfq)
outs ao,ao
endin

</CsInstruments>
; ==============================================
<CsScore>
f 0 z
i 1 0 -1
// score


</CsScore>
</CsoundSynthesizer>
  """;

  String _csd = _exampleCsd;

  String _selectedPath = "No file";
  Map<String, String> _pathMap = {"No file": "No file"};
  Map<String, DropdownMenuItem<String>> _itemList = {"No file": DropdownMenuItem<String>(child: Text("No file"), value: "No file",)};

  static const Map<String, Color> _colorMap = {
    "blue": Colors.blue,
    "red": Colors.red,
    "yellow": Colors.yellow,
    "pink": Colors.pink,
    "orange": Colors.orange,
    "brown": Colors.brown,
    "white": Colors.white,
    "black": Colors.black,
    "grey": Colors.grey,
    "green": Colors.green,
    "light_green": Colors.lightGreen,
    "light_blue": Colors.lightBlue,
    "purple": Colors.purple,
  };

  Map<String, Color> _rndColors = {};
  Color? getColor(String c, String name, bool initValues)
  {
    if(c == "random") {
      if(initValues) {
        _rndColors[name] = _colorMap.values.toList()[Random().nextInt(_colorMap.length - 1)];
      }
        return _rndColors[name];
    }
    if(!_colorMap.containsKey(c)) return null;
    return _colorMap[c];
  }


  void initState()
  {
    super.initState();
    noFile();
    processWidgets(initValues: true, csd: _csd);
    _cs = Csound();
  }

  List<Widget> _widgetList = [];
  Map<String, double> _valueMap = {};

  String controlString = "";
  String flutterString = "";
  String finalControlString = "";
  String finalCsd = "";

  Widget content = Container();


  void setControlChannel(String name, double value)
  {
    if(!_isPlaying) return;
    _cs.setControlChannel(name, value);
  }

  void initCsoundChannels()
  {
    _valueMap.forEach((key, value) {
      _cs.setControlChannel(key, value);
    });
  }

  List<Widget> processWidgets({bool initValues = false, required String csd, bool example_file = false})
  {
    //Column col = Column();
    if(initValues == true) {
      _valueMap = {};
      _rndColors = {};
      finalControlString = "";
    }

    _widgetList = [];
    if( (!csd.contains("<CsControls>")) || ((_selectedPath == "No file" ) && (example_file == false)) ) {
      finalCsd = _csd;
      return [Text("No widgets in this CSD file")];
    }

    //print("ok changing");
    //print(csd);

    controlString = csd.substring(csd.indexOf("<CsControls>") + ("""<CsControls>""").length, csd.indexOf("""<\/CsControls>"""));
    Map<String, dynamic> map = jsonDecode(controlString);

    map.forEach((key, value) {
     // print("key : value $key :: $value");
      _widgetList.add(_parse(key, value, initValues: initValues));
    });

    if(initValues) {
      finalCsd = csd.replaceFirst(csd.substring(csd.indexOf("<CsControls>"), csd.indexOf("""<\/CsControls>""") + ("""<\/CsControls>""").length), "");
    }

    return _widgetList;
  }

  Widget _parse(String name, Map<String, dynamic> content, {bool initValues = false})
  {
    final String type = content[TYPE].toString();

    if(type == ROW) {
      //Row row = Row();
      List<Widget> list = [];
      content.forEach((key, value) {
        if(key != TYPE) {
          list.add(_parse(key, value));
          list.add(Container(width: 5, color: Colors.transparent,));
        }
      });
      return Row(children: list, mainAxisAlignment: MainAxisAlignment.center,);
    } else if(type == COLUMN) {
      //Column col = Column();
      List<Widget> list = [];
      content.forEach((key, value) {
        if(key != TYPE) {
          list.add(_parse(key, value));
          list.add(Container(height: 5, color: Colors.transparent,));
        }
      });
      return Column(children: list, mainAxisAlignment: MainAxisAlignment.center,);
    } else {

      double? width = content.containsKey(WIDTH) ? double.parse(content[WIDTH].toString()) : null;
      double? height = content.containsKey(HEIGHT) ? double.parse(content[HEIGHT].toString()) : null;

      //Color? color = (content.containsKey(BACKGROUND_COLOR) && _colorMap.containsKey(content[BACKGROUND_COLOR].toString()) )
      //    ? _colorMap[content[BACKGROUND_COLOR].toString()] : null;

      Color? back_color = content.containsKey(BACKGROUND_COLOR)  ? getColor(content[BACKGROUND_COLOR].toString(), name, initValues) : null;

      return Container(
        width: width,
        height: height,
        color: back_color,
        child: InputDecorator(
          decoration: InputDecoration(

            labelText: name.toString(),
            labelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,

            ),
            alignLabelWithHint: true,
            floatingLabelBehavior: FloatingLabelBehavior.auto,

          ),


          child: _parseWidget(name, content, initValues: initValues, width:  width, height: height),
        ),
      );
      return _parseWidget(name, content);
    }
  }

  Widget _parseWidget(String name, Map<String, dynamic> content, {bool initValues = false, double? width, double? height})
  {
    final String type = content[TYPE].toString();
   // print("Type : $type | content $content");
    if(type == SLIDER)
    {
      if(initValues == true || !_valueMap.containsKey(name)) {
        setState(() {
          _valueMap[name] = double.parse(content[VALUE].toString());
        });
        this.setControlChannel(name, _valueMap[name]!);
      }
      return Column(
        children: [
          Container(height: 10), Container(height: 1, width: (width == null) ? null : width / 2, color: Colors.black,), Container(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
                Text(
                  "${content[MIN].toString()}",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${_valueMap[name]!.toStringAsFixed(3)}",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${content[MAX].toString()}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
            ],
          ),
        /*
        FlutterSlider(
          min: double.parse(content[MIN].toString()),
          max: double.parse(content[MAX].toString()),
          values: [_valueMap[name]!],
          step: (content.containsKey(STEP)) ? FlutterSliderStep(step: double.parse(content[STEP].toString())) : FlutterSliderStep(step: 0.01),
          handlerAnimation: FlutterSliderHandlerAnimation(
            curve: Curves.easeInExpo,
            duration: Duration(milliseconds: 500),
            scale: 1.5,
          ),
          //label: _valueMap[name]!.toString(),
          //divisions: (content.containsKey(DIVISIONS) ? int.parse(content[DIVISIONS].toString()) : null),
        )
         */

        SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTrackColor: Colors.red[700],
          inactiveTrackColor: Colors.red[100],
          trackShape: RoundedRectSliderTrackShape(),
          trackHeight: 4.0,
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
          thumbColor: Colors.redAccent,
          overlayColor: Colors.red.withAlpha(32),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
          tickMarkShape: RoundSliderTickMarkShape(),
          activeTickMarkColor: Colors.red[700],
          inactiveTickMarkColor: Colors.red[100],
          valueIndicatorShape: PaddleSliderValueIndicatorShape(),
          valueIndicatorColor: Colors.redAccent,
          valueIndicatorTextStyle: TextStyle(
            color: Colors.white,
          ),
        ),
      child: Slider(
        onChanged: (double v) {
          setState(() {
            _valueMap[name] = v;
          });
          this.setControlChannel(name, v);
          },
        min: double.parse(content[MIN].toString()),
        max: double.parse(content[MAX].toString()),
        value: _valueMap[name]!,
        label: _valueMap[name]!.toString(),
        divisions: (content.containsKey(DIVISIONS) ? int.parse(content[DIVISIONS].toString()) : null),
      )
      )
    ],
      );

    } else if(type == SPINBOX)
    {
      if(initValues == true || !_valueMap.containsKey(name)) {
        _valueMap[name] = double.parse(content[VALUE].toString());
        this.setControlChannel(name, _valueMap[name]!);
      }
      return SpinBox(
        onChanged: (double v){
          setState(() {
            _valueMap[name] = v;
          });
          this.setControlChannel(name, v);
        },
        min: double.parse(content[MIN].toString()),
        max: double.parse(content[MAX].toString()),
        value: _valueMap[name]!,
        step: double.parse(content[STEP].toString()),
        decimals: (content.containsKey(DECIMALS) ? int.parse(content[DECIMALS].toString()) : 1),
      );
    } else if(type == BUTTON)
    {
      if(initValues == true || !_valueMap.containsKey(name)) {
        _valueMap[name] = 0;
        this.setControlChannel(name, 0);
      }

          
      return Container(
        padding: EdgeInsets.only(top: 10, bottom: 10),
          height: height,
          width: width,
          child: ElevatedButton(
            onPressed: (){
              this.setControlChannel(name, 1);
              Timer t = Timer.periodic(BUTTON_TIMER_DURATION, (timer) {
                if(timer.tick == 10) {
                  this.setControlChannel(name, 0);
                }
              });
            },
            child: Text(name),
          )
          );

    } else if(type == TOGGLE)
    {
      if(initValues == true || !_valueMap.containsKey(name)) {
        _valueMap[name] = double.parse(content[VALUE].toString());
        this.setControlChannel(name, _valueMap[name]!);
      }
      return FlutterSwitch(
        width: 80,
        height: 40,
        value: ( (_valueMap[name]!) == 1),
        onToggle: (bool value) {
          setState(() {
            _valueMap[name] = (value == true) ? 1.0 : 0.0;
          });
          this.setControlChannel(name, _valueMap[name]!);
          },
      );
    } else if(type == RANGE)
    {
      final String low_name = "$name-low";
      final String high_name = "$name-high";
      if(initValues == true || !_valueMap.containsKey(low_name)) {
        _valueMap[low_name] = double.parse(content[VALUE_LOW].toString());
        _valueMap[high_name] = double.parse(content[VALUE_HIGH].toString());
        this.setControlChannel(name, _valueMap[low_name]!);
        this.setControlChannel(name, _valueMap[high_name]!);
      }


      return Column(
        children: [
          Container(height: 10), Container(height: 1, width: (width == null) ? null : width / 2, color: Colors.black,), Container(height: 10),
          Container(
            width: width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "${content[MIN].toString()}",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${_valueMap[low_name]!.toStringAsFixed(3)} < ${_valueMap[high_name]!.toStringAsFixed(3)}",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "${content[MAX].toString()}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
          ),

          /*
          Container(
            width: MediaQuery.of(context).size.width,
            child: Text("${content[MIN].toString()} \t < \t ${_valueMap[low_name]!.toStringAsFixed(3)} \t < \t ${_valueMap[high_name]!.toStringAsFixed(3)} \t < \t ${content[MAX].toString()}" ,
              textAlign: TextAlign.center,
              style: TextStyle(

                fontSize: 15,
                fontWeight: FontWeight.bold,

              ),
              ),

          ),
          */
          SliderTheme(
            data: SliderTheme.of(context).copyWith(
              activeTrackColor: Colors.red[700],
              inactiveTrackColor: Colors.red[100],
              trackShape: RoundedRectSliderTrackShape(),
              trackHeight: 4.0,
              thumbShape: RoundSliderThumbShape(enabledThumbRadius: 12.0),
              thumbColor: Colors.redAccent,
              overlayColor: Colors.red.withAlpha(32),
              overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
              tickMarkShape: RoundSliderTickMarkShape(),
              activeTickMarkColor: Colors.red[700],
              inactiveTickMarkColor: Colors.red[100],
              valueIndicatorShape: PaddleSliderValueIndicatorShape(),
              valueIndicatorColor: Colors.redAccent,
              valueIndicatorTextStyle: TextStyle(
                color: Colors.white,
              ),
            ),
            child: RangeSlider(
              values: RangeValues(_valueMap[low_name]!, _valueMap[high_name]!),
              min: double.parse(content[MIN].toString()),
              max: double.parse(content[MAX].toString()),
              onChanged: (RangeValues v)
              {
                setState(() {
                  _valueMap[low_name] = v.start;
                  _valueMap[high_name] = v.end;
                });
                this.setControlChannel(low_name, v.start);
                this.setControlChannel(high_name, v.end);
              },
            ),
          )
        ],
      );

    }

    throw("Invalid widget");
  }


  void noFile()
  {
    setState(() {
      _itemList= {"No file": DropdownMenuItem<String>(child: Text("No file"), value: "No file",)};
      _pathMap = {"No file": "No file"};
      _selectedPath = "No file";
    });
  }

  void changeSelection(String sel)
  {
    //print("selected $sel");
    if(sel == null || sel.isEmpty) return;
    setState(() {
      _selectedPath = sel;
    });
    if(_selectedPath != "No file") {
      setState(() {
        this._csd = File(_pathMap[_selectedPath]!).readAsStringSync();
      });
      setState(() {
        processWidgets(initValues: true, csd: _csd);
      });
    }
  }

  Future<int> _testCsd() async
  {
    double res = -1;

    String orcToEval = finalCsd.substring(finalCsd.indexOf("<CsInstruments>") + ("<CsInstruments>").length, finalCsd.indexOf("<\/CsInstruments>"));
    orcToEval += "\n\nreturn0";

    res = await _cs.evalCode(orcToEval);

    return res.toInt();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purpleAccent,
        title: Text(widget.title),
        leading: IconButton(icon: Icon(Icons.folder_open),
          tooltip: "Load file or folder (extensions .csd, .acab)",
          onPressed: () async {
          Directory root = await getApplicationDocumentsDirectory();
            String? path = await FilesystemPicker.open(
              title: 'Save to folder',
              context: context,
              rootDirectory: root,
              fsType: FilesystemType.all,
              pickText: 'Save file to this folder',
              folderIconColor: Colors.teal,
            );
            //print("path : $path");
            if(path != null) {
              if(Directory(path).existsSync()) {
                Directory dir = Directory(path);
                setState(() {
                  _pathMap = {};
                  _itemList = {};
                });
                dir.listSync(recursive: false).forEach((FileSystemEntity element) {
                  if(File(element.path).existsSync() && (element.path.toLowerCase().endsWith(".csd") || element.path.toLowerCase().endsWith("acab")) ) {
                    setState(() {
                      String filename = element.path.substring(element.path.lastIndexOf("/") + 1 );
                      _pathMap[filename] = element.path;
                      _itemList[filename] = DropdownMenuItem<String>(child: Text(filename), value: filename,);
                    });
                  }
                });
                if(_pathMap.isEmpty) {
                  noFile();
                } else {
                  setState(() {
                    _selectedPath = _pathMap.keys.toList().first;
                  });
                }
                //print("is dir");
              } else if(File(path).existsSync()){
                if(path.toLowerCase().endsWith(".csd") || path.toLowerCase().endsWith("acab") ) {
                  String filename = path.substring(path.lastIndexOf("/") + 1 );
                 // print("$filename is valid file");
                  setState(() {
                    _itemList = {filename: DropdownMenuItem<String>(child: Text(filename), value: filename,)};
                    _pathMap = {filename: path};
                    _selectedPath = filename;
                  });
                } else {
                  noFile();
                }
               // print("is file");
              }
              changeSelection(_selectedPath);
              //processWidgets(initValues: true);
            }
        },),
        actions: [
          IconButton(onPressed: (){
            setState(() {
              _csd = _exampleCsd;
            });
            this.processWidgets(initValues: true, csd: _exampleCsd, example_file: true);

          }, icon: Icon(Icons.help), tooltip: "Load example CSD",),

          DropdownButton<String>(
            items: _itemList.values.toList(),
            value: _selectedPath,
            onChanged: (String? sel)
            {
              changeSelection(sel!);
            },
          ),


          IconButton(
            icon: Icon(Icons.clear),
            onPressed: (){
              exit(0);
            },
            tooltip: "Exit app",
          )
        ],
      ),
      body: Center(

        child: Containerprint(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: this.processWidgets(csd: _csd),
            ),
          )
        )

      ),


      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            left: 30,
              bottom: 20,
              child: FloatingActionButton(
                backgroundColor: Colors.greenAccent,
                onPressed: () {
                  this.changeSelection(_selectedPath);
                },
                tooltip: "Reload current file",
                child: Icon(Icons.timer),
              ),
          ),
          Positioned(
            right: 30,
            bottom: 20,
            child: FloatingActionButton(
              onPressed: () async {
                if(!_isPlaying) {

                  //print(finalCsd);
                  int res = await _testCsd();
                  if(res != 0) {
                    throw("Csound error");
                  }
                  _cs.compileCsdText(this.finalCsd);
                  _cs.perform();
                  initCsoundChannels();
                  setState(() {
                    _isPlaying = true;
                  });
                } else {
                  _cs.stop();
                  setState(() {
                    _isPlaying = false;
                  });
                }
              },
              tooltip: 'Play/Stop',
              child: _isPlaying ? Icon(Icons.stop) : Icon(Icons.play_arrow),
            ),
          ),
        ],
      ),
      /*

      */
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
