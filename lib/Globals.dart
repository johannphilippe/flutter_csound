
const String SLIDER = "slider";
const String SPINBOX = "spinbox";
const String BUTTON = "button";
const String TOGGLE = "toggle";
const String RANGE = "range";
const String ROW = "row";
const String COLUMN = "column";


const String MIN = "min";
const String MAX = "max";
const String VALUE = "value";
const String NAME = "name";
const String STEP = "step";
const String VARIABLE = "variable";
const String DECIMALS = "decimals";
const String TYPE = "type";
const String DIVISIONS = "divisions";
const String VALUE_LOW = "low_value";
const String VALUE_HIGH  = "high_value";

const String WIDTH = "width";
const String HEIGHT = "height";
const String BACKGROUND_COLOR = "background_color";
const String FOREGROUND_COLOR = "foreground_color";

const Duration BUTTON_TIMER_DURATION = Duration(milliseconds: 20);