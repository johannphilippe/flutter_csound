# flutter_csound

Flutter interprated GUI for Csound based on JSON syntax.

## Getting Started

Somewhere in your Csound `.csd` file, add a `<CsControls>` section. 
It is based on JSON syntax. You then add some widgets. Allowed widgets types are : 

- Row : An horizontal container for widgets. 
- Column : A vertical container for widgets. Note that the main layout is vertical. 
- Slider : An horizontal slider.
- Range Slider
- Spinbox : A Number box with + and - buttons.
- Button : A pretty text button.
- Toggle : A on/off switch. 

The values of widgets will be sent as Csound channels, and retrievable with `chnget:k` inside Csound, using the name of the widget.

```
<CsControls>
{
  "first_row": {
    "type": "row",
    
    "slcontrol": {
      "type": "slider",
      "min": 200,
      "max": 500, 
      "value": 200,
      "width": 400
    },
    
    "first_button": {
      "type": "button"
    },
    
    "first_spin": {
      "type": "spinbox",
      "min": 1.0,
      "max": 5.0,
      "step": 1.5,
      "value": 1.0,
      "width": 150,
      "decimals": 1
    }
  },
  
  "sec_slider": {
    "type": "slider",
    "min": 10,
    "max": 200, 
    "step": 10,
    "value": 12
  },
  "mytoggle": {
    "type": "toggle",
    "value": 1
  }
  
}
</CsControls>
```

## TODO

- Implement Linear/Log/Exp mode for Sliders
- TouchPad
- Knob
- Oscilloscope
- VueMeter
- Other displays
- Envelop system (for gliss like Baiser Royal)
- Eval Csound code to prevent crash (show error line)
- Code editor for simple edits (need to find a virtual keyboad)
- Multiple switch (switch with multiple choices : label and value or string channel with label)